#Variables
projects=() #Ingresa el proyecto o los proyectos que deseas clonar
#Lista de proyectos: Back SAC SIS SISClientV1 VSIP autoseg dashboard gvprocesosautomaticos inventariofisico micrositio reportes sas seguridad sers shs sicop sisclientv1_ca sitio-ventura svinventarioapp svmensajeria svreportes svservicios venturaback via vpco vrec vsads erv

#ruta de descarga
path_projects="/c/Users/saranza/Documents/pruebaJS"

#Descargar Proyectos
download_projects() {
    echo 'Los proyectos se guardaran en el siguiente path: '$path_projects
    cd $path_projects
    for i in "${projects[@]}"; do
        echo "$i"
        git clone https://gitlab.com/grupo-ventura/$i.git
    done

    ls -la $path_projects
}

#Actualización de los Proyectos
update_projects() {
    for i in "${projects[@]}"; do
        #echo 'Proyecto '$i && cd $path_projects'/'$i'/' && git config --local user.email klopez@g-ventura.com
        echo 'Proyecto '$i && cd $path_projects'/'$i'/' && git pull && git remote prune origin
    done
}

#Ultimos commits por ramas
last_commits_for_branch() {
    for i in "${projects[@]}"; do

        cd $path_projects'/'$i'/'
        ramas=$(git branch --all | grep 'remotes')
        ramas=$(echo -e "${ramas// /}")                     #Delete Spaces
        ramas=$(echo -e "${ramas//'remotes/origin/'/}")     #Delete text 'remotes/origin'
        ramas=$(echo -e "${ramas//'HEAD->origin/master'/}") #Delete text 'HEAD->origin/master'
        ramas=$(echo -e "${ramas//master/}")                #Delete text 'master'
        ramas=$(echo -e "${ramas//develop/}")               #Delete text 'develop'

        #echo -e "${ramas}"
        branches=(${ramas// / })

        if [ -z "$branches" ]; then
            i=0
        else
            echo 'Proyecto '$i
            for j in "${branches[@]}"; do
                #Mostrar ultimo commit de cada una de las ramas
                git log -1 origin/$j --pretty='%Cred%h%Creset | %C(auto)%d%Creset %s | %Cgreen%ad %C(bold blue) | %an%Creset'''
            done
        fi
    done
}

#Realizar respaldo de los proyectos

if [ $1 == 'descargar' ]; then
    download_projects
fi

if [ $1 == 'actualizar' ]; then
    update_projects
fi

if [ $1 == 'commits' ]; then
    last_commits_for_branch
fi
