# Git

## Installation

```bash
projects=(Back SAC SIS SISClientV1 VSIP autoseg dashboard gvprocesosautomaticos inventariofisico micrositio reportes sas seguridad sers shs sicop sisclientv1_ca sitio-ventura svinventarioapp svmensajeria svreportes svservicios venturaback via vpco vrec vsads erv)
path_projects="[/k/Grupo-Ventura/Proyectos]"
```
## Usage

```bash
sh main.sh descargar
sh main.sh actualizar
sh main.sh commits
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
